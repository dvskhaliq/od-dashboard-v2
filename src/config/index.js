import time from './time'
import icons from './icons'
import theme from './theme'

export default {
  time,
  icons,
  theme
}
