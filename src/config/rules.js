export default {
  mustEmpty: [(v) => !v || 'Tambahkan hasil input dengan klik ikon tambah disamping kanan'],
  notEmpty: [(v) => !!v || 'Input diperlukan'],
  isObject: [(v) => !!Object.keys(v).length || 'Input diperlukan'],
  lengthMust8: [(v) => !!v && v.length === 8 || 'Harus terdiri dari 8 digit'],
  lengthMin20: [(v) => !!v && v.length > 20 || 'Harus terdiri dari minimal 20 karakter'],
  lengthMax100: [(v) => !!v && v.length < 101 || 'Harus terdiri dari maksimal 100 karakter'],
  lengthMax255: [(v) => !!v && v.length < 256 || 'Harus terdiri dari maksimal 255 karakter'],
  onlyNumber: [(v) => /^\d+$/.test(v) || 'Hanya terdiri dari angka'],
  emailFormat: [
    (v) => !v || /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'E-mail must be valid'
  ]
}
