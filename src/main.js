import Vue from 'vue'
import AppVue from './app.vue'
import router from './router'
import store from './store'

// PLUGINS
import vuetify from './plugins/vuetify'
import './plugins/http'
import './plugins/animate'
import './plugins/vue-head'
import './plugins/vuetify'
import './plugins/vcurrency'
import './plugins/clipboard'
// FILTER
import './filters/capitalize'
import './filters/lowercase'
import './filters/uppercase'
import './filters/formatCurrency'
import './filters/formatDate'
// STYLE
import './assets/scss/theme.scss'
// ANIMATION
import 'animate.css/animate.min.css'

Vue.config.productionTip = false

// const app = store.dispatch('init').then(() => {
//   return new Vue({
//     vuetify,
//     router,
//     store,
//     render: (h) => h(AppVue)
//   }).$mount('#app')
// }).then((res) => res)

store.dispatch('init')
const app = new Vue({
  vuetify,
  router,
  store,
  render: (h) => h(AppVue)
}).$mount('#app')

export default app
