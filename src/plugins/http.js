import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

const root = process.env.VUE_APP_BACKEND_ENDPOINT

// axios.defaults.withCredentials = true
axios.defaults.baseURL = root

// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
// axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
// axios.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET,POST,OPTIONS,DELETE,PUT,HEAD' 
// axios.defaults.headers.common['Access-Control-Allow-Credentials'] = true
// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

axios.interceptors.request.use(
  (config) => {
    config.headers.common['X-Requested-With'] = 'XMLHttpRequest'
    config.headers.common['Access-Control-Allow-Origin'] = '*'
    config.headers.common['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    config.headers.common['Access-Control-Allow-Methods'] = 'GET,POST,OPTIONS,DELETE,PUT,HEAD' 
    config.headers.common['Access-Control-Allow-Credentials'] = true
    config.headers.common['X-Requested-With'] = 'XMLHttpRequest'
    const token = localStorage.getItem('accessToken')
    
    if (token) {
      // axios.defaults.baseURL = root
      // axios.defaults.headers.common['Authorization'] = `AR-JWT ${token}`
      // axios.defaults.headers.common['Access-Control-Allow-Credentials'] = true
      config.baseURL = root
      config.headers.common['Authorization'] = `AR-JWT ${token}`
    }

    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

Vue.use(VueAxios, axios)

export default {
  root: root
}
