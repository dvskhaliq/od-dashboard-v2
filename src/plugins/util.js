export default function (rootUrl, payload) {
  const { page, itemsPerPage, search, sortBy, sortOrder } = payload
  const queryParam = []

  if (page) {
    console.log({ no: 1, page: page })
    queryParam.push(`page=${page}`)
  }

  if (itemsPerPage) {
    console.log({ no: 2, itemsPerPage: itemsPerPage })

    queryParam.push(`itemsPerPage=${itemsPerPage}`)
  }

  if (search) {
    console.log({ no: 3, search: search })

    queryParam.push(`search=${search}`)
  }

  if (sortBy) {
    console.log({ no: 4, sortBy: sortBy })

    queryParam.push(`sortBy=${sortBy}`)
  }

  if (sortOrder) {

    console.log({ no: 5, sortOrder: sortOrder })
    queryParam.push(`sortOrder=${sortOrder}`)
  }

  queryParam.forEach((url, index) => {
    if (index === 0) {
      rootUrl = `${rootUrl}?${url}`
    } else {
      rootUrl = `${rootUrl}&${url}`
    }
  })

  return rootUrl
}