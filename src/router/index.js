import Vue from 'vue'
import Router from 'vue-router'

// Routes
import authRoutes from './slug/auth.routes'
import dashboardRoutes from './slug/dashboard.routes'

Vue.use(Router)

const routes = [
  ...authRoutes,
  ...dashboardRoutes
]

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) return savedPosition

    return { x: 0, y: 0 }
  },
  routes
})

const validate = () => {
  const token = localStorage.getItem('accessToken')

  if (token) {
    return true
  }

  return false
}

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record)) {
    // USER OR GUEST ACCESS A VALID PATH
    if (
      to.matched.some((record) => record.meta.auth) &&
      validate()
    ) {
      // AUTHENTICATED USER ACCESS RESTRICTED PAGE

      return next()
    } else if (
      to.matched.some((record) => record.meta.auth) && !validate()
    ) {
      // GUEST ACCESS RESTRICTED PAGE      

      return next({ name: 'auth-signin' })
    } else if (
      !to.matched.some((record) => record.meta.auth) && validate()
    ) {
      // AUTHENTICATED USER ACCESS UNRESTRICTED PAGE      

      return next({ name: 'dashboard' })
    } else {
      // GUEST ACCESS UNRESTRICTED PAGE      

      return next()
    }
  } else {
    // USER OR GUEST ACCESS AN INVALID PATH
    if (validate()) {

      // AUTHENTICATED USER WILL ALWAYS BE REDIRECTED TO DASHBOARD
      return next({ name: 'dashboard' })
    } else {
      // USER AND GUEST CONTINUE ACCESS
      return next({ name: 'auth-signin' })
    }
  }
})

export default router
