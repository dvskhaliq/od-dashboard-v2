export default [
  {
    path: '/auth/signin',
    name: 'auth-signin',
    component: () =>
      import(/* webpackChunkName: "login" */ '@/pages/auth/signin-page.vue'),
    meta: {
      layout: 'auth',
      auth: false
    }
  },
  {
    path: '/auth/not-found',
    name: 'auth-error',
    component: () =>
      import(
        /* webpackChunkName: "error-not-found" */ '@/pages/error/not-found-page.vue'
      ),
    meta: {
      layout: 'error',
      auth: false
    }
  }
]
