export default [
  {
    path: '/portal/dashboard',
    name: 'dashboard',
    component: () =>
      import(/* webpackChunkName: "dashboard" */ '@/pages/portal/dashboard/dashboard-page.vue'),
    meta: {
      layout: 'portal',
      auth: true
    }
  },
  {
    path: '/portal/dashboard/incident/form',
    name: 'incident-form',
    component: () =>
      import(/* webpackChunkName: "incident-form" */ '@/pages/portal/dashboard/form/incident-form-page.vue'),
    meta: {
      layout: 'portal',
      auth: true
    }
  }
]
