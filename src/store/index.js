import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import config from '../config'
import router from '../router'

//Modules
import auth from './modules/auth'
import dashboard from './modules/dashboard'
import incident from './modules/incident'

// CONFIG
const { time } = config

Vue.use(Vuex)

const state = {
  time,
  toast: {
    show: false,
    color: 'black',
    message: '',
    timeout: 3000
  },
  profileDialogFlag: false
}

const getters = {
  profileDialogFlag : (state) => state.profileDialogFlag,
  time: (state) => state.time,
  toast: (state) => state.toast
}

const actions = {

  async init({ dispatch }) {
    console.log('Application is initiated')
    try {
      const response = await axios.get('arsys/v1.0/entry', { timeout: 5000 })

      console.log({ initdata: response.data, status: 'BMC Remedy endpoint health status is OK!' })

      dispatch('showToast', {
        type: 'success',
        timeout: 5000,
        message: '[Success] BMC Remedy endpoint health status is OK!'
      })
      await dispatch('auth/refresh', null, { root: true })
    } catch (err) {
      if (err.isAxiosError && err.response !== undefined) {
        const resTmp = err.response

        dispatch('httpErrorHandler', resTmp)
      } else {
        dispatch('systemErrorHandler', err)
      }
    }
  },

  systemErrorHandler({ dispatch }, err) {
    console.log( { systemError: err })
    dispatch('showToast', {
      type: 'fatal',
      timeout: 5000,
      message: '[FATAL] Kesalahan fatal! Harap menghubungi pembina aplikasi'
    })
  },

  httpErrorHandler({ dispatch }, response) {
    const code = response.status
    const defaultMessage = `${response.data[0].messageType}:${response.data[0].messageNumber} - ${response.data[0].messageText} - ${response.data[0].messageAppendedText}`

    console.log({ httpError: response })
    if (code === 401) {
      dispatch('showToast', {
        type: 'failure',
        timeout: 5000,
        message: `[FAILURE UNAUTHORIZED] : ${defaultMessage}`
      })
      localStorage.removeItem('accessToken')
      localStorage.removeItem('loginId')
      router.push({ name: 'auth-signin' })
    } else if ( code === 404 ) {
      dispatch('showToast', {
        type: 'failure',
        timeout: 5000,
        message: `[FAILURE NOT FOUND] : ${defaultMessage}`
      })
    } else if (code === 400 ) {
      dispatch('showToast', {
        type: 'failure',
        timeout: 5000,
        message: `[FAILURE BAD REQUEST] : ${defaultMessage}`
      })
    } else if (code === 408 ) {
      dispatch('showToast', {
        type: 'failure',
        timeout: 5000,
        message: `[FAILURE REQUEST TIMEOUT] : ${defaultMessage}`
      })
    } else {
      dispatch('showToast', {
        type: 'fatal',
        timeout: 5000,
        message: `[FATAL UNDEFINED] : ${defaultMessage}, Contact IT CSI Team!`
      })
      localStorage.removeItem('accessToken')
      localStorage.removeItem('loginId')
      router.push({ name: 'auth-signin' })
    }
  },

  showToast({ state, commit }, payload) {
    const { type, message, timeout } = payload
    let color = 'black'

    /*eslint indent: ["error", 2, { "SwitchCase": 1 }]*/
    switch (type) {
      case 'success':
        color = 'green'
        break
      case 'warning':
        color = 'orange'
        break
      case 'failure':
        color = 'red'
        break
      case 'fatal':
        color = 'purple'
        break
      default:
        color = 'black'
        break
    }

    if (state.toast.show) commit('HIDE_TOAST')
    setTimeout(() => {
      commit('SHOW_TOAST', {
        color,
        message,
        timeout
      })
    })
  },
  setProfileDialog({ commit }, flag) {
    commit('SHOW_PROFILE_DIALOG', flag)
  }
}

const mutations = {
  SHOW_TOAST(state, toast) {
    const { color, timeout, message } = toast

    state.toast = {
      message,
      color,
      timeout,
      show: true
    }
  },

  HIDE_TOAST(state) {
    state.toast.show = false
  },

  RESET_TOAST(state) {
    state.toast = {
      show: false,
      color: 'black',
      message: '',
      timeout: 3000
    }
  },
  
  SHOW_PROFILE_DIALOG(state, flag) {
    state.profileDialogFlag = flag
  }
}

export default new Vuex.Store({
  strict: false,
  state,
  getters,
  mutations,
  actions,
  modules: {
    'auth': auth,
    'dashboard': dashboard,
    'incident': incident
  }
})
