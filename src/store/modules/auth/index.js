import axios from 'axios'
import ENUM from '../../enum'
import router from '../../../router'

const state = {
  user: {},
  api: ENUM.GOOD
}

const getters = {
  api: (state) => state.api,
  user: (state) => state.user
}

const actions = {
  async refresh({ commit, dispatch }) {
    try {
      const token = localStorage.getItem('accessToken')
      const loginId = localStorage.getItem('loginId')
      
      if (token) {
        const response = await axios.get(`arsys/v1/entry/CTM:People?limit=1&fields=values(First Name,Last Name,Full Name,Remedy Login ID,Internet E-mail,Corporate ID,License Type,Company,Organization,Department,Person ID)&q='Remedy Login ID'="${loginId}"`)

        commit('SET_USER', response.data.entries[0].values)
      }
    } catch (err) {
      if (err.isAxiosError) {
        const resTmp = err.response

        dispatch('httpErrorHandler', resTmp, { root: true })
      } else {
        dispatch('systemErrorHandler', err, { root: true })
      }
    }
  },

  async signin({ commit, dispatch }, payload) {
    commit('SET_API', ENUM.LOAD)
    try {
      const params = new URLSearchParams()

      delete axios.defaults.headers.common['Authorization']
      params.append('username',payload.username)
      params.append('password',payload.password)
      params.append('authString','authenticationstring')

      const response = await axios.post('jwt/login', params, {
        headers: {
          'Content-Type' : 'application/x-www-form-urlencoded'
        }
      })

      localStorage.setItem('accessToken', response.data)
      localStorage.setItem('loginId',payload.username)
      commit('SET_API', ENUM.GOOD)
      router.push({ name: 'dashboard' })
      await dispatch('refresh')
    } catch (err) {
      if (err.isAxiosError) {
        const resTmp = err.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_API', ENUM.FAIL)
      } else {
        dispatch('systemErrorHandler', err, { root: true })
        commit('SET_API', ENUM.FAIL)
      }
    }
  },

  async logout({ commit, dispatch }) {
    try {
      const response = await axios.post('jwt/logout')

      delete axios.defaults.headers.common['Authorization']
      localStorage.removeItem('accessToken')
      localStorage.removeItem('loginId')
      commit('SET_USER', {})
      router.push({ name: 'auth-signin' })
    } catch (err) {
      if (err.isAxiosError) {
        dispatch('httpErrorHandling', err, { root: true })
      } else {
        dispatch('systemErrorHandler', err, { root: true })
      }
    }
  }
}

const mutations = {
  SET_API(state, data) {
    state.api = data
  },
  SET_USER(state, data) {
    state.user = data
  }
}

export default {
  namespaced: true,
  actions,
  state,
  mutations,
  getters
}
