import ENUM from '../../enum'
import axios from 'axios'
import moment from 'moment-timezone'

const defaultState = () => {
  return {
    header:[
      {
        text: 'Incident No.',
        value: 'incidentNumber',
        sortable: false,
        width: '12%',
        align: 'left'
      }
      ,
      {
        text: 'Severity',
        value: 'priority',
        sortable: false,
        width: '5%',
        align: 'center'
      },
      {
        text: 'Status',
        value: 'status',
        sortable: false,
        width: '10%',
        align: 'center'
      },
      {
        text: 'Summary',
        value: 'description',
        sortable: false,
        width: '10%',
        align: 'left'
      },
      {
        text: 'Impact',
        value: 'impact',
        sortable: false,
        width: '7%',
        align: 'left'
      },
      {
        text: 'Suspect',
        value: 'cause',
        sortable: false,
        width: '8%',
        align: 'left'
      },
      {
        text: 'PIC',
        value: 'assignee',
        sortable: false,
        width: '7%',
        align: 'center'
      },
      {
        text: 'Category Cause',
        value: 'categoryCause',
        sortable: false,
        width: '8%',
        align: 'center'
      },
      {
        text: 'Escalation Start',
        value: 'incTimestampStarted',
        sortable: false,
        width: '8%',
        align: 'center'
      },
      {
        text: 'Escalation End',
        value: 'incTimestampFinished',
        sortable: false,
        width: '8%',
        align: 'center'
      },
      {
        text: 'Downtime',
        value: 'incDowntimeEstimation',
        sortable: false,
        width: '5%',
        align: 'left'
      },
      {
        text: 'Action',
        value: 'action',
        sortable: false,
        width: '4%'
      }
    ],
    breadcrumbs: {
      list: [
        { text: 'Incident', href: '/portal/dashboard' },
        { text: 'Daftar' }
      ]
    },
    item: {
      entryId:null,
      incidentNumber:null,
      priority:null,
      description:null,
      impact:null,
      cause:null,
      assignee:null,
      categoryCause:null,
      incTimestampStarted:null,
      incTimestampFinished:null,
      incDowntimeEstimation:null,
      serviceCI:null,
      resolution:null,
      status:null,
      detailedDescription: null,
      thirdPartyPIC: null,
      statusReason :null
    }
  }
}

const state = {
  item: defaultState().item,
  items: [],
  totalItems: 1000,
  api: ENUM.GOOD,
  waDialogFlag: false,
  wlDialogFlag: false,
  rslvDialogFlag: false,
  statusLogs: ''
}

const getters = {
  statusLogs: (state) => state.statusLogs,
  waDialogFlag : (state) => state.waDialogFlag,
  wlDialogFlag : (state) => state.wlDialogFlag,
  rslvDialogFlag : (state) => state.rslvDialogFlag,
  item: (state) => state.item,
  breadcrumbs: () => defaultState().breadcrumbs,
  totalItems: (state) => state.totalItems,
  header: () => defaultState().header,
  items: (state) => state.items,
  api: (state) => state.api
}

const actions = {
  async fetch({ commit,dispatch }, pagePayload) {
    commit('SET_API', ENUM.LOAD)
    const skip = (pagePayload.page - 1) * 10
    let searchParam

    try {
      if (pagePayload.searchQuery === '' || pagePayload.searchQuery === null) {
        searchParam = `&q='Company' = "PT. BANK MANDIRI, TBK" AND 'Assigned Group' = "IT OCD - COMMAND CENTER"&limit=10&offset=${ skip }`
      } else {
        if (pagePayload.searchQuery.includes('INC00')) {
          searchParam = `&q='Company' = "PT. BANK MANDIRI, TBK" AND 'Assigned Group' = "IT OCD - COMMAND CENTER" AND 'Incident Number' = "${ pagePayload.searchQuery }"&limit=1`
        } else {
          searchParam = `&q='Company' = "PT. BANK MANDIRI, TBK" AND 'Assigned Group' = "IT OCD - COMMAND CENTER" AND ('Description' LIKE "%${ pagePayload.searchQuery }%" OR 'Short Description' LIKE "%${ pagePayload.searchQuery }%")&limit=10&offset=${ skip }`
        }
      }
      const response = await axios.get(`arsys/v1/entry/HPD:Help Desk?fields=values(Status_Reason, Downtime, MDR_3rdParty_PIC, Detailed Decription, Entry ID,Incident Number, Status, First Name, Last Name, Web Incident ID, Priority, MDR_Impact, Description, Short Description, MDR_DateIncStarted, MDR_DateIncFinished, Assignee, Matrix Root Cause__c, MDR_Cause, ServiceCI, Resolution)&sort=Submit Date.desc${ searchParam }`)

      commit('SET_ITEMS',response.data.entries)
      commit('SET_API',ENUM.GOOD)
    } catch (error) {
      if (error.isAxiosError) {
        const resTmp = error.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_API', ENUM.FAIL)
      } else {
        dispatch('systemErrorHandler', error, { root: true })
        commit('SET_API', ENUM.FAIL)
      }
    }
  },
  async getIncWorkLogs({ dispatch,rootGetters }, incidentNumber) {
    await dispatch('incident/getIncWorkLogs',incidentNumber, { root:true })

    return rootGetters['incident/workLogItems']
  },
  resetState({ commit }) {
    commit('RESET_STATE')
  },
  setWaDialogFlag({ commit }, { flag, item, statusLogs }) {
    commit('SET_WADIALOG_FLAG_AND_ITEM', { flag, item, statusLogs })
  },
  setWlDialogFlag({ commit }, { flag, item }) {
    commit('SET_WLDIALOG_FLAG_AND_ITEM', { flag, item })
  },
  setRslvDialogFlag({ commit }, { flag, item }) {
    commit('SET_RSLVDIALOG_FLAG_AND_ITEM', { flag, item })
  },
  wlArrayToString({ commit },wlArraySource) {
    let logToString = ''

    wlArraySource.reverse().forEach( (item) => {
      logToString += `[${ moment(item.workLogSubmitDate).format('MM/DD/YYYY HH:mm') }] ${ item.detailedDescription } \n`
    })

    commit('SET_STATUS_LOGS', logToString)
  },
  setStatusLogs({ commit }, statusLogs) {
    commit('SET_STATUS_LOGS', statusLogs)
  }
}

const mutations = {
  RESET_STATE(state) {
    state.item = defaultState().item
    state.items = []
    state.api = ENUM.GOOD
    state.totalItems = 1000
  },
  
  SET_API(state, payload) {
    state.api = payload
  },

  SET_ITEMS(state, payload) {
    const tempItems = []

    payload.forEach((item) => {
      tempItems.push({
        entryId:item.values['Entry ID'],
        incidentNumber:item.values['Incident Number'],
        priority:item.values['Priority'],
        description:item.values['Description'],
        impact:item.values['MDR_Impact'],
        cause:item.values['MDR_Cause'],
        assignee:item.values['Assignee'],
        categoryCause:item.values['Matrix Root Cause__c'],
        incTimestampStarted:item.values['MDR_DateIncStarted'] !== null ? moment(item.values['MDR_DateIncStarted']).format('YYYY-MM-DDTHH:mm') : null,
        incTimestampFinished:item.values['MDR_DateIncFinished'] !== null ? moment(item.values['MDR_DateIncFinished']).format('YYYY-MM-DDTHH:mm') : null,
        incDowntimeEstimation:item.values['Downtime'],
        serviceCI:item.values['ServiceCI'],
        resolution:item.values['Resolution'],
        status:item.values['Status'],
        detailedDescription: item.values['Detailed Decription'],
        thirdPartyPIC: item.values['MDR_3rdParty_PIC'],
        statusReason : payload['Status_Reason']
      })
    })
    state.items = tempItems
  },

  SET_STATUS_LOGS(state, statusLogs) {
    state.statusLogs = statusLogs
  },

  SET_WADIALOG_FLAG_AND_ITEM(state, { flag,item,statusLogs }) {
    state.waDialogFlag = flag
    state.item = item
    state.statusLogs = statusLogs
  },

  SET_WLDIALOG_FLAG_AND_ITEM(state, { flag,item }) {
    state.wlDialogFlag = flag
    state.item = item
  },

  SET_RSLVDIALOG_FLAG_AND_ITEM(state, { flag,item }) {
    state.rslvDialogFlag = flag
    state.item = item
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}