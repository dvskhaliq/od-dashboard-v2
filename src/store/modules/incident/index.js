import ENUM from '../../enum'
import axios from 'axios'
import moment from 'moment-timezone'

const defaultState = () => {
  return {
    workLogHeader:[
      {
        text: 'Detailed Description (Update Progress/ Status)',
        value: 'detailedDescription',
        sortable: false,
        width: '70%',
        align: 'left'
      },
      {
        text: 'Submitter',
        value: 'submitter',
        sortable: false,
        width: '15%',
        align: 'center'
      },
      {
        text: 'Work Log Submit Date',
        value: 'workLogSubmitDate',
        sortable: false,
        width: '15%',
        align: 'center'
      }
    ],
    breadcrumbs: {
      add: [
        { text: 'Incident', href: '/portal/dashboard' },
        { text: 'Add Incident' }
      ],
      edit: [
        { text: 'Incident', href: '/portal/dashboard' },
        { text: 'Edit Incident' }
      ]
    },
    item: {
      entryId:null,
      incidentNumber:null,
      priority:null,
      description:null,
      impact:null,
      cause:null,
      assignee:null,
      categoryCause:null,
      incTimestampStarted:null,
      incTimestampFinished:null,
      incDowntimeEstimation:null,
      serviceCI:null,
      resolution:null,
      status:null,
      detailedDescription: null,
      thirdPartyPIC: null,
      statusReason : null
    },
    workLogItem: {
      workLogId: null,
      viewAccess: null,
      secureWorkLog: null,
      workLogType: null,
      submitter: null,
      workLogSubmitDate: null,
      description: null,
      detailedDescription: null
    }
  }
}

const state = {
  item: defaultState().item,
  workLogItem: defaultState().workLogItem,
  workLogHeader: defaultState().workLogHeader,
  api: ENUM.GOOD,
  worklogApi: ENUM.GOOD,
  sericeCIApi: ENUM.GOOD,
  refServiceCI: [],
  workLogItems: []
}

const getters = {
  item: (state) => state.item,
  api: (state) => state.api,
  breadcrumbs: () => defaultState().breadcrumbs,
  refServiceCI: (state) => state.refServiceCI,
  serviceCIApi: (state) => state.sericeCIApi,
  worklogApi: (state) => state.worklogApi,
  workLogItem: (state) => state.workLogItem,
  workLogItems: (state) => state.workLogItems,
  workLogHeader: (state) => state.workLogHeader
}

const actions = {
  async store({ commit,dispatch,rootGetters }, payload) {
    commit('SET_API', ENUM.LOAD)
    const user = rootGetters['auth/user']
    let impact, urgency

    switch (payload.priority) {
    case 'Critical':
      impact = '1-Massive'
      urgency = '1-Urgent'
      break
    case 'High':
      impact = '2-Large'
      urgency = '2-High'
      break
    case 'Medium':
      impact = '3-Limitted'
      urgency = '3-Medium'
      break
    case 'Low':
      impact = '4-Localized'
      urgency = '4-Low'
      break
    default:
      impact = '4-Localized'
      urgency = '4-Low'
      break
    }
    try {
      const parsedPayload = { 
        'Person ID' : user['Person ID'],
        'First_Name' : user['First Name'],
        'Last_Name' : user['Last Name'],
        'Service_Type' : 'User Service Restoration',
        'Status' : 'In Progress',
        'Impact': impact,
        'ServiceCI': payload.serviceCI,
        'Urgency': urgency,
        'MDR_Impact': payload.impact,
        'Description': payload.description,
        'MDR_DateIncStarted': payload.incTimestampStarted,
        'Reported Source': 'Officer on Duty',
        'Assigned Support Company': 'PT. BANK MANDIRI, TBK',
        'Assigned Support Organization': 'IT INFRASTRUCTURE GROUP',
        'Assigned Group': 'IT OCD - COMMAND CENTER',
        'Assigned Group ID': 'SGP000000003114',
        'Assignee': user['Full Name'],
        'Assignee Login ID' : user['Remedy Login ID'],
        'Assigned To' : user['Remedy Login ID'],
        'Matrix Root Cause__c': payload.categoryCause,
        'MDR_Cause': payload.cause,
        'MDR_3rdParty_PIC': payload.thirdPartyPIC
      }

      const response = await axios.post('arsys/v1/entry/HPD:IncidentInterface_Create?fields=values(Incident Number)',{ values: parsedPayload })

      commit('SET_API', ENUM.GOOD)
      dispatch('callToast',{ type:'success', text:`[Success] : ${response.data.values['Incident Number']} has been created!` })
      dispatch('dashboard/wlArrayToString',rootGetters['incident/workLogItems'], { root:true })
      dispatch('dashboard/setWaDialogFlag', { flag:true, item: { ...payload,...{ incidentNumber:response.data.values['Incident Number'] } }, statusLogs: rootGetters['dashboard/statusLogs']  }, { root:true })

      return response
    } catch (error) {
      if (error.isAxiosError) {
        const resTmp = error.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_API', ENUM.FAIL)

        return resTmp
      } else {
        dispatch('systemErrorHandler', error, { root: true })
        commit('SET_API', ENUM.FAIL)
        
        return error
      }
    }
  },
  async update({ commit,dispatch,rootGetters }, payload) {
    commit('SET_API', ENUM.LOAD)
    const user = rootGetters['auth/user']
    let impact, urgency, priority

    switch (payload.priority) {
    case 'Critical':
      impact = '1-Extensive/Widespread'
      urgency = '1-Critical'
      priority = 'Critical'
      break
    case 'High':
      impact = '2-Significant/Large'
      urgency = '2-High'
      priority = 'High'
      break
    case 'Medium':
      impact = '3-Moderate/Limited'
      urgency = '3-Medium'
      priority = 'Medium'
      break
    case 'Low':
      impact = '4-Minor/Localized'
      urgency = '4-Low'
      priority = 'Low'
      break
    default:
      impact = '4-Minor/Localized'
      urgency = '4-Low'
      priority = 'Low'
      break
    }
    try {
      const parsedPayload = {
        'Status' : payload.status,
        'Impact': impact,
        'ServiceCI': payload.serviceCI,
        'Urgency': urgency,
        'Priority': priority,
        'MDR_Impact': payload.impact,
        'Description': payload.description,
        'MDR_DateIncStarted': payload.incTimestampStarted,
        'MDR_DateIncFinished': payload.incTimestampFinished,
        'Downtime': payload.incDowntimeEstimation,
        'Reported Source': 'Officer on Duty',
        'Assigned Support Company': 'PT. BANK MANDIRI, TBK',
        'Assigned Support Organization': 'IT INFRASTRUCTURE GROUP',
        'Assigned Group': 'IT OCD - COMMAND CENTER',
        'Assigned Group ID': 'SGP000000003114',
        'Assignee': user['Full Name'],
        'Assignee Login ID' : user['Remedy Login ID'],
        'Matrix Root Cause__c': payload.categoryCause,
        'MDR_Cause': payload.cause,
        'Resolution': payload.resolution,
        'Status_Reason' : 'No Further Action Required',
        'MDR_3rdParty_PIC': payload.thirdPartyPIC
      }

      const response = await axios.put(`arsys/v1/entry/HPD:Help Desk/${ payload.entryId }`,{ values: parsedPayload })
      
      commit('SET_API', ENUM.GOOD)
      dispatch('callToast',{ type: 'success', text: `[Success] : ${payload.incidentNumber} has been updated!` })
      dispatch('dashboard/wlArrayToString',rootGetters['incident/workLogItems'], { root:true })
      dispatch('dashboard/setWaDialogFlag', { flag:true, item: payload, statusLogs: rootGetters['dashboard/statusLogs'] }, { root:true })

      return response
    } catch (error) {
      if (error.isAxiosError) {
        const resTmp = error.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_API', ENUM.FAIL)

        return resTmp
      } else {
        dispatch('systemErrorHandler', error, { root: true })
        commit('SET_API', ENUM.FAIL)
        
        return error
      }
    }
  },
  async reset({ commit }) {
    commit('RESET_ITEM')
    commit('RESET_WORKLOG_ITEM')
    commit('RESET_WORKLOG_ITEMS')
    commit('RESET_REF_SERVICECI')
    commit('SET_API', ENUM.GOOD)
    commit('SET_WORKLOG_API',ENUM.GOOD)
    commit('SET_SERVICECI_API', ENUM.GOOD)
  },
  async find({ commit, dispatch }, entryId) {
    commit('SET_API', ENUM.LOAD)
    try {
      const response = await axios.get(`arsys/v1/entry/HPD:Help Desk/${ entryId }?limit=10&fields=values(Status_Reason, Entry ID, Incident Number, First Name, Last Name, Web Incident ID, Priority, MDR_Impact, Description, Detailed Decription, MDR_DateIncStarted, MDR_DateIncFinished, Status, Assignee, Matrix Root Cause__c, MDR_Cause, ServiceCI, Resolution, MDR_3rdParty_PIC)`)

      if (response.data.values['Status'] === 'Closed' || response.data.values['Status'] === 'Cancelled') {
        dispatch('callToast',{ type:'warning', text:`[Warning] : Inc No. ${ response.data.values['Incident Number']} currently Closed/ Cancelled, you only can view the incident!` })
      }
      
      commit('SET_ITEM',response.data.values)
      commit('SET_API',ENUM.GOOD)
    } catch (error) {
      if (error.isAxiosError) {
        const resTmp = error.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_API', ENUM.FAIL)
      } else {
        dispatch('systemErrorHandler', error, { root: true })
        commit('SET_API', ENUM.FAIL)
      }
    }
  },
  async getIncWorkLogs({ commit,dispatch }, incidentNumber) {
    commit('SET_WORKLOG_API',ENUM.LOAD)
    try {
      const response = await axios.get(`arsys/v1/entry/HPD:WorkLog?fields=values(Secure Work Log,View Access, Work Log ID, Description,Detailed Description, Work Log Submit Date, Submitter, Work Log Type)&q='Work Log Type' = "General Information" AND 'Incident Number' = "${ incidentNumber }"`) //'Work Log Type' = "General Information" AND 

      commit('SET_WORKLOG_ITEMS',response.data.entries)
      commit('SET_WORKLOG_API',ENUM.GOOD)
    } catch (error) {
      if (error.isAxiosError) {
        const resTmp = error.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_WORKLOG_API',ENUM.FAIL)
      } else {        
        commit('SET_WORKLOG_API',ENUM.FAIL)
      }
    }
  },
  async addIncWorkLog({ commit }, { incidentNumber, detailedDescription }) {
    commit('SET_WORKLOG_API',ENUM.LOAD)
    try {
      const parsedPayload = {
        'Incident Number': incidentNumber,
        'Work Log Type': 'General Information',
        'View Access': 'Public',
        'Secure Work Log': 'No',
        'Detailed Description': detailedDescription,
        'Description': 'Worklog from OD ' + incidentNumber
      }
      const response = await axios.post('arsys/v1/entry/HPD:WorkLog',{ values: parsedPayload })

      commit('SET_WORKLOG_ITEMS',response.data.entries)
      commit('SET_WORKLOG_API',ENUM.GOOD)
    } catch (error) {
      if (error.isAxiosError) {
        const resTmp = error.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_WORKLOG_API',ENUM.FAIL)
      } else {
        commit('SET_WORKLOG_API',ENUM.FAIL)
      }
    }
  },
  async getRefServiceCI({ commit }, searchKey) {
    commit('SET_SERVICECI_API',ENUM.LOAD)
    try {
      const response = await axios.get(`arsys/v1/entry/BMC.CORE:BMC_BusinessService?fields=values(RequestId, Name, DatasetId)&q='DatasetId' = "BMC.ASSET" AND 'ParentCITag' != $NULL$ AND 'Company' = "PT. BANK MANDIRI, TBK" AND 'Name' LIKE "%${ searchKey }%"`)

      commit('SET_REF_SERVICECI',response.data.entries)
      commit('SET_SERVICECI_API',ENUM.GOOD)
    } catch (error) {
      
      if (error.isAxiosError) {
        const resTmp = error.response

        dispatch('httpErrorHandler', resTmp, { root: true })
        commit('SET_SERVICECI_API',ENUM.FAIL)
      } else {        
        commit('SET_SERVICECI_API',ENUM.FAIL)
      }
    }
  },
  callToast({ dispatch },{ text, type }) {
    dispatch('showToast',{
      type: type,
      timeout: 5000,
      message: text
    }, { root:true })
  }
}

const mutations = {
  SET_ITEM(state, payload) {
    state.item.entryId = payload['Entry ID']
    state.item.incidentNumber = payload['Incident Number']
    state.item.priority = payload['Priority']
    state.item.description = payload['Description']
    state.item.impact = payload['MDR_Impact']
    state.item.cause = payload['MDR_Cause']
    state.item.assignee = payload['Assignee']
    state.item.categoryCause = payload['Matrix Root Cause__c']
    state.item.incTimestampStarted = payload['MDR_DateIncStarted'] !== null ? moment(payload['MDR_DateIncStarted']).format('YYYY-MM-DDTHH:mm') : null
    state.item.incTimestampFinished = payload['MDR_DateIncFinished'] !== null ? moment(payload['MDR_DateIncFinished']).format('YYYY-MM-DDTHH:mm') : null
    state.item.incDowntimeEstimation = payload['Downtime']
    state.item.resolution = payload['Resolution']
    state.item.serviceCI = payload['ServiceCI']
    state.item.status = payload['Status'],
    state.item.detailedDescription = payload['Detailed Decription'],
    state.item.thirdPartyPIC = payload['MDR_3rdParty_PIC'],
    state.item.statusReason = payload['Status_Reason']
  },

  SET_WORKLOG_ITEMS(state, payload) {
    const tempItems = []

    payload.forEach((item) => {
      tempItems.push({
        workLogId: item.values['Work Log ID'],
        viewAccess: item.values['View Access'],
        secureWorkLog: item.values['Secure Work Log'],
        workLogType: item.values['Work Log Type'],
        submitter: item.values['Submitter'],
        workLogSubmitDate: item.values['Work Log Submit Date'],
        description: item.values['Description'],
        detailedDescription: item.values['Detailed Description']
      })
    })

    state.workLogItems = tempItems
  },

  RESET_ITEM(state) {
    state.item = defaultState().item
  },

  RESET_WORKLOG_ITEM(state) {
    state.workLogItem = defaultState().workLogItem
  },

  RESET_WORKLOG_ITEMS(state) {
    state.workLogItems = []
  },

  RESET_REF_SERVICECI(state) {
    state.refServiceCI = []
  },

  SET_REF_SERVICECI(state, payload) {
    const tempRefServiceCI = []

    payload.forEach((item) => {
      tempRefServiceCI.push(item.values['Name'])
    })
    state.refServiceCI = tempRefServiceCI
  },

  SET_API(state, payload) {
    state.api = payload
  },

  SET_SERVICECI_API(state, payload) {
    state.sericeCIApi = payload
  },

  SET_WORKLOG_API(state, payload) {
    state.worklogApi = payload
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}